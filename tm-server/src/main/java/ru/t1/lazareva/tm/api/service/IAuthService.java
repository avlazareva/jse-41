package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.SessionDTO;
import ru.t1.lazareva.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token) throws Exception;

    void invalidate(@Nullable final SessionDTO session) throws Exception;

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

}