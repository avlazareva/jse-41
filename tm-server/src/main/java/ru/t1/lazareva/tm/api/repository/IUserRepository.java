package ru.t1.lazareva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.DBConstants;
import ru.t1.lazareva.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm.tm_user (id, login, password, email, locked, " +
            "first_name, last_name, middle_name, role)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{locked}, " +
            "#{firstName}, #{lastName}, #{middleName}, #{role})")
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm.tm_user")
    void clear();

    @Select("SELECT * FROM tm.tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = DBConstants.COLUMN_PASSWORD),
            @Result(property = "firstName", column = DBConstants.COLUMN_FIRST_NAME),
            @Result(property = "lastName", column = DBConstants.COLUMN_LAST_NAME),
            @Result(property = "middleName", column = DBConstants.COLUMN_MIDDLE_NAME)
    })
    @Nullable List<UserDTO> findAll();

    @Select("SELECT * FROM tm.tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = DBConstants.COLUMN_PASSWORD),
            @Result(property = "firstName", column = DBConstants.COLUMN_FIRST_NAME),
            @Result(property = "lastName", column = DBConstants.COLUMN_LAST_NAME),
            @Result(property = "middleName", column = DBConstants.COLUMN_MIDDLE_NAME)
    })
    @Nullable UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm.tm_user WHERE LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = DBConstants.COLUMN_PASSWORD),
            @Result(property = "firstName", column = DBConstants.COLUMN_FIRST_NAME),
            @Result(property = "lastName", column = DBConstants.COLUMN_LAST_NAME),
            @Result(property = "middleName", column = DBConstants.COLUMN_MIDDLE_NAME)
    })
    @Nullable UserDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm.tm_user")
    int getSize();

    @Delete("DELETE FROM tm.tm_user WHERE id = #{id}")
    void remove(@NotNull UserDTO user);

    @Update("UPDATE tm.tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, role = #{role} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Select("SELECT * FROM tm.tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = DBConstants.COLUMN_PASSWORD),
            @Result(property = "firstName", column = DBConstants.COLUMN_FIRST_NAME),
            @Result(property = "lastName", column = DBConstants.COLUMN_LAST_NAME),
            @Result(property = "middleName", column = DBConstants.COLUMN_MIDDLE_NAME)
    })
    @Nullable UserDTO findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm.tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = DBConstants.COLUMN_PASSWORD),
            @Result(property = "firstName", column = DBConstants.COLUMN_FIRST_NAME),
            @Result(property = "lastName", column = DBConstants.COLUMN_LAST_NAME),
            @Result(property = "middleName", column = DBConstants.COLUMN_MIDDLE_NAME)
    })
    @Nullable UserDTO findByEmail(@NotNull @Param("email") String email);

    @Select("SELECT EXISTS(SELECT 1 FROM tm.tm_user WHERE login = #{login})")
    @NotNull Boolean isLoginExist(@NotNull @Param("login") String login);

    @Select("SELECT EXISTS(SELECT 1 FROM tm.tm_user WHERE email = #{email})")
    @NotNull Boolean isEmailExist(@NotNull @Param("email") String email);

}
