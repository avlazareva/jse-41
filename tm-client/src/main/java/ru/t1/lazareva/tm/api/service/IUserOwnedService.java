package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.IUserOwnedRepository;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}